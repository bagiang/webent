<%-- 
    Document   : listProduct
    Created on : Feb 28, 2018, 4:57:17 PM
    Author     : B.Rengg
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>List All Product</title>
    </head>
    <body>
        <h1>List Product</h1>
        <jsp:useBean class="com.wpsj.model.ProductFinderBean" id="listProduct" scope="request"/>
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${listProduct.allProducts}" var="product">
                    <tr>
                        <td><c:out value="${product.id}"/></td>
                        <td><c:out value="${product.name}"/></td>
                        <td><c:out value="${product.desc}"/></td>
                        <td><a href="edit.jsp">Edit</a></td>
                        <td><a href="Xoa?id=<c:out value="${product.id}"/>">Delete</a></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </body>
</html>
