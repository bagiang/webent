<%-- 
    Document   : create
    Created on : Feb 27, 2018, 5:04:26 PM
    Author     : B.Rengg
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Create Product</title>
    </head>
    <body>
        <h1>Create</h1>
        <form action="ProductFinder" method="POST">
            <table>
                <tr>
                    <td>ID</td>
                    <td>Name</td>
                    <td>Desc</td>
                </tr>
                <tr>
                    <td><input type="text" name="pro_id"></td>
                    <td><input type="text" name="pro_name"></td>
                    <td><input type="text" name="pro_desc"></td>
                    <td><input type="submit" /></td>
                    <span style="color: red;">
                <c:out value="${param.msg}"/></span>
                </tr>
            </table>
        </form>
    </body>
</html>
