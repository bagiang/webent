<%-- 
    Document   : edit
    Created on : Mar 1, 2018, 3:22:38 PM
    Author     : B.Rengg
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <form action="Edit" method="POST">
            <table>
                <tr>
                    <td>ID</td>
                    <td>Name</td>
                    <td>Desc</td>
                </tr>
                <tr>
                    <td><input type="text" name="pro_id"></td>
                    <td><input type="text" name="pro_name"></td>
                    <td><input type="text" name="pro_desc"></td>
                    <td><input type="submit" /></td>
                </tr>
            </table>
        </form>
    </body>
</html>
