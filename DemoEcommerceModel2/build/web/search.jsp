<%-- 
    Document   : search
    Created on : Feb 27, 2018, 3:13:12 PM
    Author     : B.Rengg
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Shopping</title>
    </head>
    <body>
        <h1>Product List</h1>
        <form action="ProductFinder">
            <span style="color: red;">
                <c:out value="${param.msg}"/></span>
            <input name="name"/><input type="submit"/>
        </form>
        <form action="listProduct">
            <a href="listProduct.jsp" >ListProduct</a>
        </form>
    </body>
</html>
