/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wpsj.model;

import java.util.List;
import com.wpsj.da.ProductDataAccess;
import com.wpsj.entity.Product;
import java.sql.SQLException;
/**
 *
 * @author Truong
 */
public class ProductFinderBean {
    private  String keyword;
    
    public  void setKeyword(String keyword)
    {
        this.keyword = keyword;
    }
    
    
    public  List<Product> getProducts() throws SQLException, ClassNotFoundException
    {
        return new
            ProductDataAccess().getProductsByName(keyword);
    }
    
    public  List<Product> getAllProducts() throws SQLException, ClassNotFoundException
    {
        return new
            ProductDataAccess().getAllProduct();
    }
    
    public boolean AddProducts(Product product) throws SQLException, ClassNotFoundException {
        ProductDataAccess data = new ProductDataAccess();
        data.addProduct(product.getId(), product.getName(), product.getDesc());
        return true;
    }
    
    public void DeleteProduct(int id) throws SQLException, ClassNotFoundException {
        ProductDataAccess data = new ProductDataAccess();
        data.DeleteProduct(id);
    }
}
