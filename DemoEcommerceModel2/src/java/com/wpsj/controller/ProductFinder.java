/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wpsj.controller;

import com.wpsj.entity.Product;
import com.wpsj.model.ProductFinderBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

/**
 *
 * @author B.Rengg
 */
public class ProductFinder extends HttpServlet {

    private ProductFinderBean dao;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String keyword = request.getParameter("name");
            if (keyword == null || keyword.trim().isEmpty()) {
                response.sendRedirect("search.jsp?msg=Enter keyword pls!");
                return;
            }
            ProductFinderBean finder = new ProductFinderBean();
            finder.setKeyword(keyword);
            request.setAttribute("finder", finder);

            RequestDispatcher rd = request.getRequestDispatcher("result.jsp");
            rd.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        
        try {
            if (action.equalsIgnoreCase("listProduct")) {
                listProduct(request, response);
            }
            else if (action.equalsIgnoreCase("delete")) {
                DeleteProduct(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String pro_id = request.getParameter("pro_id");
            String pro_name = request.getParameter("pro_name");
            String pro_desc = request.getParameter("pro_desc");

            Product product = new Product();
            product.setId(Integer.parseInt(pro_id));
            product.setName(pro_name);
            product.setDesc(pro_desc);

            ProductFinderBean finder = new ProductFinderBean();
            finder.AddProducts(product);
            request.setAttribute("finder", finder);

            RequestDispatcher rd = request.getRequestDispatcher("listProduct.jsp");
            rd.forward(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ProductFinder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductFinder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void listProduct(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {
        List<Product> listProduct = dao.getAllProducts();
        request.setAttribute("listProduct", listProduct);
        RequestDispatcher rd = request.getRequestDispatcher("listProduct.jsp");
        rd.forward(request, response);
    }
    
    private void DeleteProduct(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {
        String id = request.getParameter("id");
        dao.DeleteProduct(Integer.parseInt(id));
        request.setAttribute("deleteProduct", dao);
        RequestDispatcher rd = request.getRequestDispatcher("listProduct.jsp");
        rd.forward(request, response);
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
