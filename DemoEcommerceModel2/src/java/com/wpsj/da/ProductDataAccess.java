/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wpsj.da;

import com.wpsj.entity.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Truong
 */
public class ProductDataAccess {

    private PreparedStatement searchStatement;

    private PreparedStatement getSearchStatement() throws ClassNotFoundException,
            SQLException {
        if (searchStatement == null) {
            Connection connection = new DBConnection().getConnection();
            searchStatement
                    = connection.prepareStatement("SELECT pro_id,pro_name, pro_desc FROM ProductStore WHERE pro_name like ?");

        }
        return searchStatement;
    }

    public List<Product> getProductsByName(String name) throws ClassNotFoundException, SQLException {
        try {
            PreparedStatement statement = getSearchStatement();
            statement.setString(1, "%" + name + "%");
            ResultSet rs = statement.executeQuery();
            List<Product> products = new LinkedList<Product>();
            while (rs.next()) {
                products.add(new Product(rs.getInt("pro_id"),
                        rs.getString("pro_name"), rs.getString("pro_desc")));
            }
            return products;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private PreparedStatement getAddStatement() throws ClassNotFoundException,
            SQLException {
        if (searchStatement == null) {
            Connection connection = new DBConnection().getConnection();
            searchStatement
                    = connection.prepareStatement("INSERT INTO ProductStore VALUES (?, ?, ?)");

        }
        return searchStatement;
    }

    public void addProduct(int pro_id, String pro_name, String pro_desc) throws ClassNotFoundException,
            SQLException {
        try {
            PreparedStatement statement = getAddStatement();
            statement.setInt(1, pro_id);
            statement.setString(2, pro_name);
            statement.setString(3, pro_desc);
            statement.execute();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private PreparedStatement getAllProductStatement() throws ClassNotFoundException,
            SQLException {
        if (searchStatement == null) {
            Connection connection = new DBConnection().getConnection();
            searchStatement
                    = connection.prepareStatement("SELECT * FROM ProductStore");

        }
        return searchStatement;
    }

    public List<Product> getAllProduct() throws ClassNotFoundException,
            SQLException {
        try {
            PreparedStatement statement = getAllProductStatement();
            ResultSet rs = statement.executeQuery();
            List<Product> product = new ArrayList<Product>();
            while (rs.next()) {
                Product pro = new Product();
                pro.setId(rs.getInt(1));
                pro.setName(rs.getString(2));
                pro.setDesc(rs.getString(3));
                product.add(pro);
            }
            return product;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private PreparedStatement DeleteStatement() throws ClassNotFoundException,
            SQLException {
        if (searchStatement == null) {
            Connection connection = new DBConnection().getConnection();
            searchStatement
                    = connection.prepareStatement("DELETE FROM ProductStore WHERE pro_id = ?");

        }
        return searchStatement;
    }

    public void DeleteProduct(int id) throws ClassNotFoundException,
            SQLException {
        try {
            PreparedStatement statement = DeleteStatement();
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private PreparedStatement EditStatement() throws ClassNotFoundException,
            SQLException {
        if (searchStatement == null) {
            Connection connection = new DBConnection().getConnection();
            searchStatement
                    = connection.prepareStatement("Update ProductStore Set pro_id = ?, pro_name = ?, pro_desc = ?");

        }
        return searchStatement;
    }

    public void EditProduct(int id, String name, String desc) throws ClassNotFoundException,
            SQLException {
        try {
            PreparedStatement statement = DeleteStatement();
            statement.setInt(1, id);
            statement.setString(2, name);
            statement.setString(3, desc);
            statement.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
